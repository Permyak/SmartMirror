﻿namespace WeatherAppSmartMirror
{
    using Newtonsoft.Json;
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Media.Imaging;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        string openWeatherAppId = "4564c80faf98c8f01826bc88c152c47f";

        public MainPage()
        {
            this.InitializeComponent();

            Display();
        }

        private async void Display()
        {
            txb_currentTime.Text = DateTime.Now.ToString("HH:mm");
            txb_currentDay.Text = string.Format("{0}, {1}", "Вт", "25 октября");

            var weatherData = await GetData();
            txb_temperature.Text = string.Format("{0:0}°", weatherData.main.temp);
            txb_weatherStatus.Text = weatherData.weather[0].description.Substring(0, 1).ToUpper() + weatherData.weather[0].description.Substring(1);
            //TODO: Get a city name from user settings
            txb_weatherCity.Text = "Пермь";
            img_weatherStatusIcon.Source = new BitmapImage(new Uri(this.BaseUri, string.Format("/WhiteWeatherIcons/{0}.png", weatherData.weather[0].icon)));
        }

        public async Task<OpenWeatherMapData> GetData()
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    var response = await httpClient.GetAsync(new Uri(string.Format("http://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}&units=metric&lang=ru", "Perm", openWeatherAppId)));

                    return JsonConvert.DeserializeObject<OpenWeatherMapData>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
